import streamlit as st
import boto3

st.title("Kendra Web App")

# Create a Kendra client
kendra = boto3.client("kendra")

# Provide the index ID
index_id = "b700d5ac-d3a6-46f9-9ae1-56c38d8a5935"

# Input field for the query
query = st.text_input("Enter your question:", "")

# Button to execute the query
if st.button("Search"):
    st.write(f"Search results for question: {query}\n")

    response = kendra.query(
        QueryText=query,
        IndexId=index_id
    )

    for index, query_result in enumerate(response["ResultItems"]):
        st.write(f"Answer {index+1}")

        if query_result["Type"] == "ANSWER" or query_result["Type"] == "QUESTION_ANSWER":
            answer_text = query_result["DocumentExcerpt"]["Text"]
            answer_title = query_result.get("DocumentTitle", {}).get("Text", "")
            
            # Highlight the answer title
            st.write(f"**{answer_title}**")
            st.write(answer_text)
        elif query_result["Type"] == "DOCUMENT":
            if "DocumentTitle" in query_result:
                document_title = query_result["DocumentTitle"]["Text"]
                st.write(f"Title: {document_title}")
            document_text = query_result["DocumentExcerpt"]["Text"]
            
            # Remove dots from the beginning and end of document_text
            document_text = document_text.strip(".")
            
            st.write(document_text)

        if index + 1 >= 3:
            break
