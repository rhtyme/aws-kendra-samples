Executive Summary Answer Archive

Eastern Illinois Executive Summary

SEMO

USNH

WCC

Eastern Illinois Executive Summary

Pathify drives student success by enabling higher ed institutions to provide a centralized technology experience throughout the entire student lifecycle, proactively delivering critical resources through a modern, accessible web and mobile ecosystem. Engineered to be deeper and broader than any other higher ed student portal, Pathify redefines the student portal experience, delivering easy to use capabilities digital natives expect. 

Operating since 2017, Pathify has nearly 100 full time staff and over 60 clients. Some examples of successful projects with other institutions using Banner include Johnson Wales University, Utah State University, San Jacinto Community College District, Ventura County Community College District, East Georgia State College, Antelope Valley College, University of Denver, Truman State University, Athens State University, and Alabama A M University.

The entire team at Pathify is thrilled to compete to become Eastern Illinois University s Student Engagement Platform. The growing list of institutions embracing the Student Engagement Platform as a unique medium connecting students with peers, interests, groups and content underlines the new and exciting role this critical digital infrastructure plays on modern campuses. Pathify will surpass the stated needs of the Student Engagement Platform to improve the full student journey, with the potential to extend beyond students, faculty, and staff in the future to include prospective students, alumni and even parents.

Pathify delivers a cloud based, modern web and mobile application that acts as a centralized hub, pushing the right content, messaging, and information at the right time on any device. The application dramatically improves personalized communication by leveraging extremely granular roles driven from the SIS, which also facilitates a personalized front door experience for supporting technologies, source systems, and other critical information and content. Professional, experienced, and extremely creative, the Pathify team will go above and beyond every day to help drive student success. 

Thank you for your time and consideration in this process Pathify would be thrilled to partner with Eastern Illinois University.

SEMO

Pathify drives student success by enabling higher ed institutions to provide a centralized technology experience throughout the entire student lifecycle, proactively delivering critical resources through a modern, accessible web and mobile ecosystem. Engineered to be deeper and broader than any other higher ed student portal, Pathify redefines the student portal experience, delivering easy to use capabilities digital natives expect. 

Operating since 2017, Pathify has 62 full time staff and nearly 100 clients. Some examples of successful projects with other institutions using Banner include Johnson Wales University, Utah State University, San Jacinto Community College District, Ventura County Community College District, East Georgia State College, Antelope Valley College, University of Denver, Truman State University, Athens State University, and Alabama A M University.

The entire team at Pathify is thrilled to compete to become Southeast Missouri State University s campus portal. The growing list of institutions embracing the campus portal as a unique medium connecting students with peers, interests, groups and content underlines the new and exciting role this critical digital infrastructure plays on modern campuses. Pathify will surpass the stated needs of the campus portal to improve the full student journey, with the potential to extend beyond students, faculty, and staff in the future to include prospective students, alumni and even parents.

Pathify delivers a cloud based, modern web and mobile application that acts as a centralized hub, pushing the right content, messaging, and information at the right time on any device. The application dramatically improves personalized communication by leveraging extremely granular roles driven from the SIS, which also facilitates a personalized front door experience for supporting technologies, source systems, and other critical information and content. Professional, experienced, and extremely creative, the Pathify team will go above and beyond every day to help drive student success. 

Thank you for your time and consideration in this process Pathify would be thrilled to partner with Southeast Missouri State University.

USNH

The entire team at Pathify appreciates the opportunity to participate in the University System of New Hampshire s Request for Proposal RFP 212022357. The growing list of institutions embracing the student portal as a unique medium connecting students, faculty and staff with peers, interests, groups, and content underlines the new and exciting role this critical digital infrastructure plays on modern campuses. Pathify will surpass the stated needs of the web portal to improve the full user journey , with the potential to extend beyond students, faculty, and staff in the future to include prospective students, alumni and even parents.



Pathify understands USNH seeks a Web Portal solution so students, faculty and staff will more easily find the resources they need to be successful. USNH has stated that institutional websites are more student recruitment oriented and outward facing. In addition, the large variety of websites at each USNH institution sometimes makes it difficult to know where to find critical information. To serve the community needs each USNH campus has focused on portal based solutions. Each campus currently has a different solution with separate maintenance and costs. 



After careful review of the RFP, Pathify understands in order to meet student needs across USNH, the selected solution must be flexible enough to support a multi campus environment with disparate ERP systems for HR and Finance, disconnected SIS systems Banner, Colleague , and separate Canvas LMS instances at each school. The portal solution will provide users with an appropriately branded view of services, tools, announcements, and more based on roles associated with each campus.



Pathify drives student success by enabling higher ed institutions to provide a centralized user experience hub, proactively delivering critical resources through a modern, accessible web and mobile ecosystem. Engineered to be deeper and broader than any other higher ed student portal, Pathify redefines the student user experience, driving easy to use capabilities digital natives expect.



Pathify delivers a centralized, cloud based, modern web portal and mobile application, pushing to users the right content, messaging, and information at the right time on any device. The application dramatically improves personalized communication by leveraging extremely granular roles driven from the SIS, facilitating a personalized front door experience for supporting technologies, source systems, and other critical information and content. This results in dramatically improved communication, community, data flow, student success, and integrations.



As the front door to all things digital, Pathify provides unparalleled personalized communications and enhanced opportunities for universal adoption all living behind USNH s single sign on and authentication wall. The technology features 100 parity between web and mobile devices. Native iOS and Android applications, branded for USNH, will be available in the appropriate mobile application stores Apple App Store and Google Play Store .





Beyond surfacing data and providing windows into other critical systems, the Engagement Hub becomes an extremely useful place to actually get things done. By pulling frequent activities to the center like class registration, scheduling, and communications, both students and administrators no longer need to bounce between multiple systems to accomplish tasks. And when it makes sense to send users to a source system, the Engagement Hub will play the role of a traditional portal, porting the user exactly where they need to go to accomplish a task, all operating within a consistent user experience shell.



Personalization lies at the heart of the Pathify platform nowhere more apparent than in the area of communications. Secure, highly targeted, and omnichannel, Pathify affords multiple opportunities and toolsets for effective, personalized, secure communications across all user Roles. Personalization also drives effective access to information. Users only view information germane to their Role, mitigating the information overload phenomenon burdening most traditional portals. 



Pathify makes available a truly unique capability called Communities formally called Groups designed to provide a much needed digital forum for connection. Communities enable users to connect with offices, departments, organizations, and fellow users in a structured environment. Through the use of granular permissions, students, staff, and faculty join Communities of interest restricting users from Groups that don t make sense based on their Role. Within each group users post questions and comments about Community topics to engage in meaningful conversations. Administrators create and host events through Zoom or Teams video conferencing. Users view and download resources, accessing integrated FAQ s pertaining to the Community. In this way, Communities becomes a digital representation of the physical campus at USNH, giving users a place to connect in the context of their interests. 



With a visually appealing and intuitive user experience, the Pathify portal provides simple indicators for critical communications and information, with dynamic checklist workflows as a perfect example. During implementation and beyond, dedicated customer success resources enjoy sharing new ideas and capabilities, aligning to institutional initiatives regularly. 



Pathify s proposed solution involves a single instance of Pathify for the USNH multi campus structure. Pathify supports integrations to both Banner and Colleague simultaneously, while at the same time accommodating the separate instances of Canvas utilized by each campus. Pathify will leverage and integrate best in breed single sign on SSO infrastructure per USNH requirements.



Pathify leverages SSO capabilities throughout the application, providing integrations to all source systems and 3rd party technologies accessed through the portal and mobile application. Pathify delivers the most modern, robust portal functionality available, fully addressing USNH needs as defined in the RFP. Providing a cloud based, one stop shop approach ensuring the right users access the right tools and information at the right time, a partnership with Pathify represents enormous opportunity for overall student success.





In terms of project lift, time to value, and the time it takes to get Pathify working for students, the Engagement Hub is typically up and running in three to four months. Pathify s Standard Implementation Package is designed to remove as much of the effort as possible from the institution, with a typical Go Live taking between 40 and 70 hours for schools. This is not a multi year ERP implementation, and the Pathify team handles all integrations for any system hooked into the Engagement Hub.



Professional, experienced, and extremely creative, the Pathify team will go above and beyond every day to help drive student success. The Pathify Senior Leadership Team includes former university executives, technologists, and entrepreneurs, while the Customer Success organization includes former customers who have implemented Pathify. From the moment work begins, the team will work with USNH to scope, implement, and launch something students and staff will love, while prioritizing on going strategic and operational conversations to ensure the web portal remains a positive force now and in the future.



We look forward to showing USNH how we work, but also how we will support USNH s needs through the delivery of the Pathify ultra modern engagement platform. We re thrilled to share with USNH why Pathify goes Beyond a Portal. 

WCC

Pathify drives student success by helping higher ed institutions center users on their own personalized journey, proactively delivering critical resources through a modern, accessible web and mobile ecosystem. Engineered to be deeper and broader than any other higher ed student portal, Pathify redefines the student portal experience, delivering easy to use capabilities digital natives expect. 

The entire team at Pathify is thrilled to compete to become Washtenaw Community College s student portal. The growing list of institutions that embrace the student portal as a unique medium connecting students with peers, interests, groups and content underlines the new and exciting role this critical digital infrastructure plays on modern campuses. Pathify will surpass the stated needs of the student portal to improve the full student journey , with the potential to extend beyond students, faculty, and staff in the future to include prospective students, alumni and even parents.

Pathify delivers a cloud based, modern student portal and mobile application that acts as a centralized hub, pushing to users the right content, messaging, and information at the right time on any device. The application dramatically improves personalized communication by leveraging extremely granular roles driven from the SIS, which also facilitates a personalized front door experience for supporting technologies, source systems, and other critical information and content. All living behind Washtenaw Community College s single sign on and authentication wall, the technology features 100 feature parity between web and mobile devices. Native iOS and Android applications, branded for Washtenaw Community College, will be available in the appropriate mobile application stores Apple App Store and Google Play Store .

Professional, experienced, and extremely creative, the Pathify team will go above and beyond every day to help drive student success. The Pathify Senior Leadership Team includes former university executives, technologists, and entrepreneurs, while the Customer Success organization includes former customers who have implemented Pathify. From the moment work begins, the team will partner with Washtenaw Community College to scope, implement, and launch something students and staff will love, while prioritizing on going strategic and operational conversations to ensure the student portal remains a positive force into the future. 

Thank you for your time and consideration in this process Pathify would be thrilled to partner with Washtenaw Community College.



Last reviewed 