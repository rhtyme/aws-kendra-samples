Mobile App Responsive Design Answer Archive

Mobile App Functionality

Mobile App Branding

Mobile App Accessibility

Mobile App Responsive Design

Mobile App Software Updates

Mobile App Notifications

Mobile App Functionality

Does Pathify s portal include a mobile app?

Pathify s portal consists of both a browser app and a mobile app with complete capability parity.



What differentiates Pathify s mobile app from other platforms?

With Pathify, users can launch a fully functional, accessible app on any device with complete capability parity, allowing consistent access to the same content.



Is Pathify s mobile app available for Apple and Android?

 Pathify publishes a native mobile app for Apple and Android users.

Mobile App Branding

Can customers apply their own naming and branding conventions to the mobile app?

Each customer incorporates their unique naming and branding conventions to their own mobile app, while Pathify developers send this mobile app to the respective mobile app stores.



What mobile app stores are available for the customer mobile app?

Native iOS and Android applications, branded for the customer, will be available in the appropriate mobile application stores Apple s App Store and Google Play Store .



Mobile App Accessibility

Is Pathify s mobile app ADA and WCAG compliant?

Pathify s mobile app fully complies with ADA and WCAG standards with content in the browser app automatically appearing within the mobile app.

Mobile App Responsive Design

What is Pathify s approach to responsive design regarding the mobile app?

 The Pathify platform is built on a mobile responsive code base and

offers a native mobile app reflecting 100 parity across the web and mobile experience.



Does the Pathify mobile app allow for screens with different pixel sizes?

Pathify delivers a fully responsive experience, offering easy navigation and an appealing layout to students whether their screens are 460 or 1400 pixels.

Mobile App Software Updates

Does Pathify release software updates to the mobile app?

Pathify releases software updates and enhancements that appear in both the mobile app and browser app.



When do software updates occur?

Pathify releases software updates and enhancements every two weeks, occuring on US Tuesday nights.



Are customers informed of mobile app software updates prior to release and at release?

The week before each software release, customers receive an outline of updates as well as detailed updates the day of release.



Does the mobile app undergo additional software upgrades?

 Occasionally, the mobile app undergoes additional upgrades.







Do additional software upgrades to the mobile app follow the release schedule?

Additional software updates to the mobile app do not follow the release schedule they are pushed directly to the Apple and Android app stores.



Is there downtime when a mobile software update is released?

 Pathify s software updates do not involve downtime.

Mobile App Notifications

Can customers send mobile push notifications?

Customers can send targeted mobile push notifications a channel that dramatically outperforms traditional email notifications, driving overall adoption and student engagement.



How are notifications sent to the mobile app?

Announcements and notifications in the portal trigger push notifications to users who have downloaded the mobile app.



How can portal administrators manage mobile app notifications?

Portal administrators can choose the types of Events triggering push notifications, as well as what users can opt out of.



Do users with the mobile app receive Emergency Alert Announcements?

Users with the mobile app always receive push notifications for Emergency Alert Announcements, no matter how their notification settings are configured.









Last reviewed 