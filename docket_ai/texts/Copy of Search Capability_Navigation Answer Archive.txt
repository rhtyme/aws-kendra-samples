Search Capability Navigation Answer Archive

Search Capability Navigation Content

Search Capability Navigation Algorithm

Search Capability Navigation Dashboard View

Search Capability Navigation Direct Links

Search Capability Navigation Roles Permissions

Search Capability Navigation Content

How can users find content inside and outside of the portal?

Pathify s robust Search functionality allows users to search via keyword or phrase and find content living both inside and outside the portal.



What content inside the portal is Pathify able to search using the search bar?

Pathify Search allows users to quickly find content housed throughout Tools, Groups, Posts, Files, Resources, Events, Pages, and user profiles all based on configurable permissions . 



How can portal administrators configure search capability for external content?

Portal administrators can configure external websites to be searchable within the Engagement Hub interface, empowering institutions to unlock the entire digital institutional universe. 



Does the portal have a search bar for navigation?

 Users can search for additional content in the portal Search bar.



Is Search Capability Navigation affected by device?

Pathify offers full parity between mobile and browser apps, ensuring user access to the same content no matter how one views the portal.

Search Capability Navigation Algorithm

How does Pathify s Search algorithm rank search results?

Pathify s Search algorithm ranks search results based on content title, type, body, and any attached metadata tags, referred to in the Pathify ecosystem as Labels.

Does portal Search account for text input errors?

Portal Search algorithms account for both misspellings and incomplete search terms.



How do users view search results?

Users view either all results or results sorted by category People, Groups, Pages and Tools .

Search Capability Navigation Dashboard View

For what content modules can users search on their dashboard?

On login, users see the user dashboard, which displays Widgets, upcoming Events and social Groups likely to be of interest to that user based on their role s .



How are links organized on a user s dashboard?

Users can see Quicklink Cards, or cards displaying links organized by category.



What information is contained in a Quicklink Card?

Quicklink Cards have nested links relevant to the user based on role and the time during the semester.



Can users navigate and search different modules from the front end view?

On the left hand side of the view on both web and mobile, users can access the navigation panel, where they locate role specific Groups, Pages, and Tools links organized by category.



Can portal administrators control content available on a user s left hand navigation bar?

Content creators pin certain important Groups, Pages, and Tools to the navigation bar, while users pin additional helpful resources to the navigation bar as they find them.




Search Capability Navigation Direct Links

Are users able to navigate to direct links?

Direct links, known as Tools, are part of Standard Implementation and appear on the left hand side of the portal view, where they are organized by categories.



Can Tool navigation content be controlled by roles and permissions?

The Tools that appear for users are based on roles students see Student Tools while faculty sees Tools of use to Faculty, and users with both roles see both.



Do users have to log in again to navigate to Tool direct links?

The SSO integration ensures that users do not need to log in again to access third party systems requiring authentication.

Search Capability Navigation Roles Permissions

Can Pathify Search be controlled by roles and permissions?

Pathify Search leverages the exact same permissions as the other parts of the system, restricting Search results, and allowing users with correct roles and sub roles to access the right content.



What is an example of role controlled search capability navigation?

Staff members find content living in a Page meant for staff members and these resources do not surface to students while Groups configured as private are also unfindable to non members.





Last reviewed 