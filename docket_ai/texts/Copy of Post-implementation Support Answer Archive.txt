Post implementation Support Answer Archive

Post implementation Support Portal Administrator

Post implementation Support Training Meetings

Post implementation Support Support Team

Post implementation Support Feature Requests

Post implementation Support Portal Administrator

How many staff members does Pathify recommend serving as the ongoing Portal Administrator?

Once launched, Pathify recommends maintaining at least one staff member to serve as the ongoing Portal Administrator.



How much of a full time staff member s workload is required for Portal Administrator duties?

The time typically required for a Portal Administrator to support the Pathify portal equates to roughly 20 of a full time staff member s workload.



What is the role of a Portal Administrator post launch?

The Portal Administrator often oversees implementation and serves as the central point of contact for inquiries, feedback, and technical issues related to the maintenance and promotion of Pathify.

Post implementation Support Training Meetings

What training and support resources does Pathify offer post launch?

Post launch, Pathify offers ongoing training and ongoing support meetings.



What is offered regarding ongoing training?

For ongoing training, Pathify follows a train the trainer model to ensure customers know how to maintain the portal, enabling most new staff members to learn the portal from their associates and peers.





What happens to training offered in the event of employee turnover?

In the event of employee turnover, the Pathify team provides additional Admin Plus training for the new Portal Administrators.



What is offered regarding ongoing support meetings?

For ongoing support meetings, Pathify assigns a dedicated Account Manager who conducts monthly check in meetings to all customers.



How many meetings are offered throughout a year between a customer and Account Manager?

Ongoing support meetings allow for at least 12 built in touch points throughout the year.



Are ongoing support meetings offered at an additional cost?

 Ongoing support meetings are offered at no additional cost.

Post implementation Support Support Team

Does Pathify have a support team contact?

Outside of recurring implementation meetings weekly and post implementation meetings monthly , post launch, Pathify customers can contact the Pathify Help Center via email.



What are the support team standard hours of operation?

Pathify s support offers standard hours of operation between 6 00am and 6 00pm MT Monday through Friday , excluding major US holidays.



What support is offered after hours?

After hours, Pathify s Australian employees expedite incoming tickets with Severity 1 2 to the Engineering team.



How does Pathify define a weekend and holiday?

Pathify support defines weekends as Saturday and Sunday and holidays as major US holidays, of which Pathify provides a list at the beginning of each calendar year.

Post implementation Support Feature Requests

Can customers submit feature requests?

Customers add feature requests to Canny, where other customers vote on the features they would most like to see in the portal.



Can customers submit for priority feature development?

Some customers choose to pay for priority for feature development, with these requests going through a custom scoping process.



What post launch support and software updates does Pathify offer at no additional cost?

Post launch meetings, support desk use, bug fixes, and biweekly releases are offered at no additional cost.



What is considered a chargeable request?

Chargeable requests involve custom work and or changes to the Pathify middleware configurations.



Last reviewed 