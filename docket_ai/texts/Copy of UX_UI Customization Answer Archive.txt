UX UI Customization Answer Archive

UX UI Customization Customization Approach

UX UI Customization Portal Admin Customization

UX UI Customization End User Customization

UX UI Customization User Provisioning

UX UI Customization Customization Approach

Does Pathify offer an intuitive user interface?

By leveraging a multi modal, omni channel approach, users access resources on their preferred device, giving administrators confidence students see exactly what they should, regardless of access choice.



How does Pathify offer an intuitive user interface using permissions?

The use of roles configures each individual s portal experience in their own unique view users only see Widgets, Pages, and Tools relevant to their role s on campus.



Does Pathify save user preferences for future logins?

 User preferences are stored through refreshes and logouts.



How many Widgets are included in the Standard Implementation Package for end users?

Standard Implementation includes six Widgets displaying feeds from other key applications like the SIS and LMS.



Does Pathify allow for custom Widgets to customize the end user experience?

Pathify provides the capability to scope custom Widgets to supplement the data highlighted in the portal dashboard.

UX UI Customization Portal Admin Customization

Who is able to customize notification preferences, page layout, etc. for end users?

Both portal administrators and end users are able to customize notification preferences, page layout, etc. for end users.



What can portal administrators customize for the end user experience?

Portal administrators set the ground rules for what end users configure regarding content, the navigation menu, Widgets, and notifications, and end users customize their experience within this framework.



How can portal administrators customize what content end users are able to view?

Portal administrators can customize what content end users view based on their role assignments.



How can portal administrators customize content for the end user navigation menu?

Portal administrators can customize which Pages, Tools, and Groups end users see permanently pinned to their navigation menu.



How can portal administrators customize widgets for end users?

Portal administrators can customize whether end users can move Widgets or those Widgets remain locked in place in a specified order as well as which individual Widgets end users may remove from the user dashboard.



How can portal administrators customize notifications for end users?

Portal administrators can customize how end users will receive notifications SMS, email or push notifications as well as the degree to which end users configure their notification preferences.

UX UI Customization End User Customization

What can end users customize for the end user experience?

End users can customize content, the navigation menu, Widgets, notifications, privacy settings, and their pronoun preferences.



How can end users customize content in the left hand navigation menu?

End users can configure their left hand navigation menu with the resources most useful to them, adding their most frequently visited Pages, Tools, and Groups to Shortcuts using the favorites option.





How can end users customize Widgets?

End users can determine the Widgets on their dashboard via the Widget Library and Widget order provided portal admin configured this ability .



How can end users customize their notification settings?

End users can adjust their notification settings within the parameters of what portal administrators allow.



Can end users customize Emergency Alert notifications?

Emergency Alerts sent out as push notifications override any user configured notification settings.



How can end users customize their privacy settings?

End users can update their privacy settings to determine how visible they are to other users.



How can end users customize their pronoun preferences?

End users can choose whether to display their pronouns by their name in search results and on their profile.

UX UI Customization User Provisioning

Does Pathify provide for user provisioning and onboarding?

 Pathify provides multiple routes for user provisioning and onboarding.



How can a user create their own user account?

A user can create their own account using on demand user provisioning or social authentication.



What is on demand user provisioning?

On demand user provisioning allows a user to create their own user account during their first SSO login.



How can a user create their own user account with social authentication?

Users can create their own account with social authentication through Google, Apple, or Linkedin.





What follows after the user account is created?

 User accounts are then enriched by a flat file user provisioning process.

Last reviewed 