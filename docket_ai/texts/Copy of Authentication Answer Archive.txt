Authentication Answer Archive

Authentication SSO General v1 2023 

Authentication SSO CAS SAML v2 Azure Ellucian Ethos Identity 

Authentication SSO CAS SAML v3 Shibboleth 4.31 SAML 2.0 

Authentication Authentication Methodologies

Authentication SSO General v1 2023 

What provider does Pathify use for SSO?

Generally a consumer of the institution s authentication strategy, Pathify leverages the institution s preferred SSO provider.



Does Pathify support sign on and single logout?

 Pathify supports both single sign on and single logout.



How does Pathify support SSO integration?

Pathify supports SSO integration via SAML 2.0 for the customer s preferred identity provider, including but not exclusive to Shibboleth, Azure, ADFS, Okta and Portalguard. 



Who configures session length?

 Portal administrators configure their instance s session length.



How is session length configured?

Session length is configurable per customer, ensuring that Pathify participates in secure management of user access correctly and does not expose a browser session outside of IDP management.



How does Pathify authenticate users?

Pathify consumes the customer authentication strategy including MFA and provides a SSO MFA solution when required.



What is the user experience after completing SSO integration?

After completing the SSO integration, Pathify Shortcuts, Tools, and Widgets provide one click access to any university tools and systems authenticated users are authorized to access.





Authentication CAS SAML v2 Azure Ellucian Ethos Identity 

How does Pathify authenticate users onto the portal via SAML with Azure or Ellucian Ethos Identity?

Pathify authenticates users onto the portal via SAML through the customer s preferred SSO provider.



Does Pathify honor MFA configurations set within the IdP?

 Pathify honors MFA configurations set within the IdP.



What happens after the SSO integration is complete?

After completing the SSO integration, Pathify Shortcuts, Tools, and Widgets provide one click access to any university tools and systems authenticated users are authorized to access.



What does Pathify recommend regarding Ellucian Ethos Identity and Azure for authentication?

Pathify recommends associating Ellucian Ethos Identity with Azure and then having users authenticate using Azure.



Authentication SSO CAS SAML v3 Shibboleth 4.31 SAML 2.0 

How does Pathify authenticate users onto the portal via SAML with Azure or Ellucian Ethos Identity?

Pathify authenticates users onto the portal via SAML through the customer s preferred SSO provider.



Does Pathify honor MFA configurations set within the IdP?

 Pathify honors MFA configurations set within the IdP.



What information can users access after completing SSO integration?

After completing the SSO integration, Pathify Shortcuts, Tools, and Widgets provide one click access to any university tools and systems authenticated users are authorized to access.



Authentication Authentication Methodologies

What SSO technologies and methodologies does Pathify use?

Pathify supports SAML 2.0 dialects such as ADFS, Azure AD, Google Auth, and Shibboleth.



Does Pathify support direct LDAPS queries as an authentication method?

Pathify supports direct LDAPS queries and may be used to test a user entered user password pairing, but this is not our recommended configuration.



Does Pathify support Shibboleth as an authentication method?

Shibboleth is available as a supported authentication pattern.



Does Pathify support ADFS as an authentication method?

 ADFS Saml v2.0 is available as a supported authentication pattern.



Does Pathify support local authentication as an authentication method?

Local authentication is supported, but Pathify does not recommend it for general staff and student usage.



How does Pathify manage authentication schemes?

Any authentication scheme configured for a partner is a hybrid scheme because local user accounts are also established for administrator and superuser accounts to be issued to nominated customer staff.



Is Pathify an identity provider?

Pathify is not fundamentally an identity provider but a consumer of the customer s identity provider in ADFS parlance, it is a service provider through establishment of a relying party trust.





Last reviewed October 3, 2023