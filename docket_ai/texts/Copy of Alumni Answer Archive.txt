Alumni Answer Archive

Alumni Alumni Portal Features

Alumni Alumni Portal Portal Features

What is the optional Alumni Portal add on?

The Alumni Portal is a portal view for alumni, making it easy for alumni to keep up with campus events, news, and favorite professors via direct messaging and helping them feel connected and invested by creating networking connections based on their current interests, like career, degree, location, and more.



How are customers able to manage the Alumni Portal?

Customers pull everything alumni care about into a single app, so they stay up to date and connected, keeping up with events, participating in Groups, and maintaining a presence in the community.



What do alumni see when they visit the Alumni Portal?

When alumni visit the Alumni Portal, they may see important reminders, nudges to donate to the latest fundraising drive, and can share open internships or jobs.



How can content creators put important campaigns in front of alumni?

Content creators can put important campaigns in front of alumni with in portal Announcements, push notifications and Tasks.



Last reviewed October 3, 2023